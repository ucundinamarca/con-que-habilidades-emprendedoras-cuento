
var ST = "#Stage_";
var S = "Stage_";
ivo.info({
    title: "¿Con qué habilidades emprendedoras cuento?",
    autor: "Edilson Laverde Molina",
    date:  "10/03/2021",
    email: "edilsonlaverde_182@hotmail.com",
    icon: 'https://www.ucundinamarca.edu.co/images/iconos/favicon_verde_1.png',
    facebook: "https://www.facebook.com/edilson.laverde.molina"
});
var audios=[
    {url:"sonidos/click.mp3",      name:"clic"      },
    {url:"sonidos/fail.mp3",       name:"fail"      },
    {url:"sonidos/good.mp3",       name:"good"      },
    {url:"sonidos/chicharra.mp3",  name:"chicharra" },
    {url:"sonidos/Audio_1.mp3",    name:"a1"        },
    {url:"sonidos/Audio_2.mp3",    name:"a2"        },
    {url:"sonidos/Audio_3.mp3",    name:"a3"        }
];
var textos=[
    {text:"<div style='margin-top:-300px'><h1>Objetivo</h1><p>Realizar un autodiagnóstico que permita identificar las habilidades propias de un emprendedor con las que se cuenta y aquellas que pueden llegar a desarrollarse.</p></div>"},
    {text:"<div style='margin-top:-300px'><h1>Contexto</h1><p>Existe un tipo de habilidades, denominadas blandas, que le permiten al emprendedor iniciar y sostener un proyecto hasta llevarlo al éxito.</p></div>"},
    {text:"<div><h1>Instrucción</h1><p>A través de esta actividad usted podrá identificar las habilidades propias de un emprendedor con las que cuenta y aquellas que puede llegar a desarrollar. Para conseguirlo deberá valorar numéricamente qué tanto se identifica usted con los distintos aspectos que se le presentarán en relación a las siete (7) habilidades blandas que ya conoce.</p><p>De esta manera, la sumatoria de los puntos asignados a cada uno de los tres (3) aspectos que aluden a una habilidad blanda le permitirá identificar el nivel de desarrollo por su parte de dicha habilidad.</p><p>Una vez valorada su identificación con los distintos aspectos asociados a una habilidad blanda, recibirá una práctica recomendación del paso a seguir para desarrollar o potenciar dicha habilidad. <b>¡Inicie ya su autodiagnóstico!</b></p></div>"}
]

var q=[
    {
        "t":"Apertura y adaptación al cambio",
        "n":"1",
        "q":"Esta habilidad representa la posibilidad de estar dispuesto a comprender y apreciar perspectivas diferentes, cambiar convicciones y conductas a fin de adaptarse de forma rápida y eficiente a diversas situaciones, contextos, medios y personas.",
        "options":[
            "Se me facilita identificar y comprender rápidamente los cambios en el entorno que pueden estar afectándome de manera directa en la planeación o desarrollo de mi iniciativa.",
            "Suelo transformar las debilidades en fortalezas.",
            "Muestro capacidad para improvisar frente a las dificultades que puedo enfrentar al no poder llevar a cabo un plan trazado."
        ],
        "bajo":"Debe empezar por entender qué causa el cambio, estudiar e investigar de manera constante qué consecuencias traen los cambios en el entorno. Conviene identificar claramente las debilidades para adaptarse y enfocarse en lo positivo de cambiar, tener una actitud contante de querer aprender, potenciar la capacidad de improvisar de manera controlada, sin olvidar tener un plan definido en la construcción del proyecto.",
        "alto":"Potenciar las habilidades de cambio es fundamental para la construcción de un proyecto robusto, de tal manera que se puedan sortear las posibles dificultades a enfrentar en el desarrollo del mismo. Para esto es necesario afianzar las habilidades que posee el equipo y desarrollar aún más la improvisación, capacidades fundamentales, pues es casi imposible tener cubiertas todas las bases, ya que el entorno fluctúa y nuestros clientes constantemente cambian de gustos, intereses o necesidades."
    },
    {
        "t":"Enfoque en resultados",
        "n":"2",
        "q":"Hace referencia a la capacidad que debe tener el emprendedor para centrarse en la consecución de los retos propuestos, para ello debe haber definido el curso de acción a emplear y los recursos internos y externos requeridos para seguirlo.",
        "options":[
            "Antes de realizar una acción, realizo un plan detallado de acción que sigo al pie de la letra.",
            "Suelo hacer una sola cosa a la vez centrándome en lo que debo hacer.",
            "Hago una lista de los recursos que necesito para llevar a cabo lo planeado en el marco de mi proyecto."
        ],
        "bajo":"Conviene trabajar en el diseño de un plan de acción detallado, que se debe seguir de manera ordenada, sistemática y disciplinada. Se debe optimizar el tiempo de manera eficaz, por ello se sugiere trabajar en una cosa a la vez de manera concentrada, evitando distracciones y previniendo posibles errores y reprocesos. Se aconseja al hacer la planeación hacer un listado de recursos requeridos para implementar lo planificado.",
        "alto":"Debe seguir trabajando de manera constante en el cultivo del hábito de mantenerse enfocado/a en lo que persigue."
    },
    {
        "t":"Pensamiento creativo e innovador",
        "n":"3",
        "q":"Corresponde a la capacidad para idear soluciones nuevas y diferentes dirigidas a resolver problemas o situaciones que se presentan en el desarrollo del proyecto.",
        "options":[
            "Suelo pensar en soluciones diferentes para abordar los problemas que pueda enfrentar en el desarrollo de mi emprendimiento.",
            "Usualmente intento desafiar el orden y las reglas establecidas.",
            "Generalmente investigo ampliamente sobre cómo hacer las cosas de manera diferente."
        ],
        "bajo":"Debe seguir algunas recomendaciones: Desarrollar la capacidad de observación, investigar y profundizar continuamente los temas relacionados con el desarrollo del proyecto,  preguntar, indagar, formarse, capacitarse. También se sugiere la realización de actividades de corte artístico (pintar, bailar, hacer ejercicio), pues así se estimula el hemisferio creativo.",
        "alto":"Es recomendable seguir cultivando la creatividad, siempre pensando en maneras alternativas de hacer las cosas, haciéndolas más prácticas, más sencillas y más enriquecedoras para el cliente."
    },
    {
        "t":"Aptitud de liderazgo y estilo persuasivo",
        "n":"4",
        "q":"Es la capacidad para desarrollar equipos de trabajo, tejer acuerdos y negociaciones. Implica también saber escuchar e interpretar las necesidades de los clientes y los colaboradores, y transmitir con pasión y convicción los desafíos que tenga el emprendedor para que los demás lo comprendan y sean capaces tanto de respaldar como acompañar las iniciativas y retos planteados.",
        "options":[
            "Soy capaz de lograr que los demás hagan lo que yo sugerí.",
            "Soy muy hábil transmitiendo con pasión qué es lo que se debe hacer, cuáles son los desafíos y retos.",
            "Se me hace fácil explicar las implicaciones de un proyecto y lograr que los demás respalden mis iniciativas.",
        ],
        "bajo":"Se deben desarrollar habilidades relacionadas con el liderazgo, como poder identificar el talento que las demás personas tienen, retroalimentar de manera efectiva a los miembros del equipo, buscando que no se sientan agredidos, sino acompañados y retados a ser mejores. Creer en primera instancia en sí mismo/a para ser capaz de convencer a otros.",
        "alto":"Impulsar el desarrollo de las demás personas, siempre empleando la asertividad para conseguir mejores resultados como equipo. No olvide prepararse de manera continua, recuerde que las habilidades que no se practican se pierden."
    },{
        "t":"Comunicación efectiva",
        "n":"5",
        "q":"Capacidad para escuchar y entender a clientes y miembros del equipo con el fin de transmitir de forma clara y oportuna la información requerida, buscando alcanzar los objetivos y así mantener canales de comunicación abiertos y redes de contacto formales e informales que satisfagan los requerimientos de los diferentes grupos de interés.",
        "options":[
            "Transmito con seguridad y convicción lo que quiero comunicar.",
            "Soy muy hábil escuchando y entendiendo lo que los demás necesitan.",
            "Verifico el mensaje que acabo de transmitir."
        ],
        "bajo":"Se debe desarrollar con especial cuidado la habilidad de aprender a escuchar al otro de manera atenta y consciente, demostrando empatía en todo momento. Así se es capaz de establecer un contacto inicial de calidad. Una vez hecho este primer paso, debe establecer un plan para transmitir el mensaje con seguridad y claridad. Por último, se debe verificar qué contenido del mensaje quedó claro por parte del interlocutor, mediante una pregunta simple: ¿Cuénteme qué entendió?",
        "alto":"Debe continuar desarrollando esta capacidad, siempre estableciendo de manera clara los objetivos del mensaje y cuidando los detalles a transmitir."
    },
    {
        "t":"Orientación al servicio",
        "n":"6",
        "q":"Implica la habilidad para actuar con sensibilidad ante los requerimientos de un cliente y/o conjunto de clientes, actuales o potenciales, externos o internos, que se puedan presentar en la actualidad o en el futuro. Requiere que el emprendedor tenga una vocación permanente de servicio al cliente, que le permita comprender adecuadamente sus demandas y generar soluciones efectivas a sus necesidades",
        "options":[
            "Permanezco atento/a de manera constante a lo que el cliente pueda requerir.",
            "El servicio al cliente es mi prioridad como emprendedor.",
            "Al recibir información procedente de un cliente, me aseguro de poder hacer los ajustes y modificaciones necesarias para satisfacer sus requerimientos."
        ],
        "bajo":"Debe enfocarse en buscar de manera constante lo que el cliente pueda requerir, convirtiendo el servicio al cliente en la prioridad del proyecto, en el centro. Para esto es fundamental establecer los canales de interacción con el público para lograr satisfacer sus necesidades, gustos y preferencias.",
        "alto":"Se deben poner en ejercicio mecanismos de comunicación constante con los clientes para así estar siempre atentos y expectantes a sus necesidades."
    },
    {
        "t":"Trabajo en equipo",
        "n":"7",
        "q":"Es la capacidad para colaborar con los demás, poner en segundo plano los intereses personales frente a los objetivos del proyecto. Implica tener expectativas positivas respecto a los demás, comprender a los otros, y generar y mantener un buen clima de trabajo, una buena comunicación y un alto sentido de pertenencia.",
        "options":[
            "Me parece fácil colaborar con todo tipo de personas para conseguir los objetivos propuestos.",
            "Estoy seguro/a de que los demás tienen elementos valiosos para aportar a mi idea de negocio.",
            "Usualmente pongo mis intereses personales por debajo de los del equipo."
        ],
        "bajo":"Es necesario intentar trabajar con todo tipo de personas, incluidas las que sean muy diferentes en términos de gustos y opiniones a la suya, comprenda que sus intereses deben estar por debajo de los del equipo si se trata de conseguir los resultados esperados. Aprenda a comunicarse de manera asertiva para que así estén claros los objetivos y limitaciones del equipo.",
        "alto":"Continúe trabajando en el cultivo de su capacidad para trabajar con personas diferentes, pues es una característica bastante deseable hoy en día dentro del ambiente de los negocios."
    }
]
var puntos=[0,0,0];
function main(sym) {
var index=0;
var primary=true;

var t=null;
udec = ivo.structure({
        created:function(){
           t=this;
            //precarga audios//
           ivo.load_audio(audios,onComplete=function(){
               ivo(ST+"preload").hide();
               ivo(ST+"Group4").hide();
               t.launch(index);
               t.events();
               t.animation();
               stage1.play();

           });
        },
        methods: {
            launch:function(){
                ivo(ST+"s").text(q[index].t);
                ivo(ST+"n").html(q[index].n);
                ivo(ST+"_q").html(q[index].q);
                ivo(ST+"t1").html(q[index].options[0]);
                ivo(ST+"t2").html(q[index].options[1]);
                ivo(ST+"t3").html(q[index].options[2]);
            },
            events:function(){
                var t=this;
                ivo(ST+"btn_ins").on("click",function(){
                    ivo.play("clic");
                    ivo(ST+"info").html(textos[2].text);
                }).on("mouseover", function () {ivo(this).css("opacity","0.7")}).on("mouseout", function () {ivo(this).css("opacity","1")});
                ivo(ST+"btn_con").on("click",function(){
                    ivo.play("clic");
                    ivo(ST+"info").html(textos[1].text);
                }).on("mouseover", function () {ivo(this).css("opacity","0.7")}).on("mouseout", function () {ivo(this).css("opacity","1")});
                ivo(ST+"btn_obj").on("click",function(){
                    ivo.play("clic");
                    ivo(ST+"info").html(textos[0].text);
                }).on("mouseover", function () {ivo(this).css("opacity","0.7")}).on("mouseout", function () {ivo(this).css("opacity","1")});
                ivo(ST+"btn_sig").on("click",function(){
                    stage1.timeScale(300).reverse();
                    ivo.play("clic");
                    stage2.timeScale(1).play();
                    
                });

                ivo(ST+"btn_sig2").on("click",function(){
                    stage2.timeScale(300).reverse();
                    ivo.play("clic");
                    stage3.timeScale(1).play();
                  
                });

                ivo(ST+"btn_creditos2,"+ST+"btn_creditos").on("click",function(){
                    creditos.timeScale(1).play();
                    ivo.play("clic");
                });
                ivo(ST+"creditos,"+ST+"btn_cerrar2").on("click",function(){
                    creditos.timeScale(1).reverse();
                    ivo.play("clic");
                });
                var get_numero = function(id){
                    if(id.search("1")>0){
                        return 1;
                    }
                    if(id.search("2")>0){
                        return 2;
                    }
                    if(id.search("3")>0){
                        return 3;
                    }
                    if(id.search("4")>0){
                        return 4;
                    }
                }
                ivo(".botones1").on("click",function(){
                    let n = get_numero(ivo(this).attr("id"));
                    ivo(".botones1").show();
                    ivo(this).hide();
                    puntos[0]=n;
                    ivo.play("clic");
                });
                ivo(".botones2").on("click",function(){
                    let n = get_numero(ivo(this).attr("id"));
                    ivo(".botones2").show();
                    ivo(this).hide();
                    puntos[1]=n;
                    ivo.play("clic");
                });
                ivo(".botones3").on("click",function(){
                    let n = get_numero(ivo(this).attr("id"));
                    ivo(".botones3").show();
                    ivo(this).hide();
                    puntos[2]=n;
                    ivo.play("clic");
                });
                ivo(ST+"btn_evaluar").on("click",function(){
                    let total = puntos[0] + puntos[1] + puntos[2];
                    if(puntos[0]!=0 && puntos[1]!=0 && puntos[2]!=0){
                        if(total<=7){
                            ivo(ST+"retro").html(q[index].bajo);
                            ivo(ST+"Text4").text("Nivel bajo").css("color","#D63B3B");
                          
                          
                        }else{
                            ivo(ST+"retro").html(q[index].alto);
                            ivo(ST+"Text4").text("Nivel alto").css("color","#39B54A");
                       
                        }
                        ivo(ST+"nd").text(total);
                        ivo(".botones1, .botones2, .botones3").show();
                        ivo(ST+"Group3").hide();
                        ivo(ST+"Group4").show();
                        ivo.play("clic");
                    }else{
                        ivo.play("chicharra");
                    }
                });
                ivo(ST+"btn_sig3").on("click",function(){
                    puntos[0]=0;
                    puntos[1]=0;
                    puntos[2]=0;
                    index+=1;
                    ivo.play("clic");
                    if(index==7){
                        stage4.play();
                    }else{
                        t.launch();
                        ivo(ST+"Group3").show();
                        ivo(ST+"Group4").hide();
                    }
                });
                ivo(ST+"btn_instruccion").on("click",function(){
                    ivo.play("clic");
                    pop.play().timeScale(1);
                });
                ivo(ST+"btn_cerrar").on("click",function(){
                    ivo.play("clic");
                    pop.play().reverse(10);
                });
                ivo(ST+"btn_play").on("click",function(){
                    ivo.play("a2");
                });
            },
            animation:function(){
                stage1 = new TimelineMax();
                stage1.append(TweenMax.from(ST+"stage1", .8,          {x:1300,opacity:0}), 0);
                stage1.append(TweenMax.from(ST+"Recurso_1", .8,       {x:1300,opacity:0}), 0);
                stage1.append(TweenMax.staggerFrom(".destelloz",.4,   {x:10,opacity:0,ease:Elastic.easeOut.config(0.3, 0.4)},.2), 0);
                stage1.append(TweenMax.from(ST+"Recurso_8", .8,       {scale:0,rotation:900,opacity:0}), 0);
                stage1.append(TweenMax.from(ST+"Recurso_9", .8,       {y:900}), 0);
                stage1.append(TweenMax.from(ST+"Recurso_9", .8,       {x:100}), 0);
                stage1.append(TweenMax.from(ST+"btn_sig", .8,         {x:100,opacity:0}), -0.5);
                stage1.stop();

                stage2 = new TimelineMax({onStart:function(){
                    if(primary){
                        $(ST+"btn_obj").trigger("click");
                        primary=false;
                    }else{
                        $(ST+"btn_ins").trigger("click");
                    }
                    ivo.play("a1");
                }});
                stage2.append(TweenMax.from(ST+"stage2", .8,          {x:1300,opacity:0}), 0);
                stage2.append(TweenMax.from(ST+"Recurso_11", .8,      {x:1300,opacity:0}), 0);
                stage2.append(TweenMax.from(ST+"Recurso_12", .8,      {y:1300,opacity:0}), 0);
                stage2.append(TweenMax.from(ST+"box", .8,             {x:1300,opacity:0}), 0);
                stage2.append(TweenMax.staggerFrom(".btn",.4,         {x:100,opacity:0,ease:Elastic.easeOut.config(0.3, 0.4)},.2), 0);
                stage2.append(TweenMax.from(ST+"btn_creditos", .8,    {y:300,opacity:0}), 0);
                stage2.append(TweenMax.from(ST+"btn_sig2", .8,        {x:300,opacity:0}), 0);
                //stage2.append(TweenMax.staggerFrom(".btn",.4,         {x:300,ease:Elastic.easeOut.config(0.3, 0.4)},.2), 0);
                stage2.stop();

                stage3 = new TimelineMax({onStart:function(){
                    ivo.play("a2");
                }});
                stage3.append(TweenMax.fromTo(ST+"stage3", .4,        {opacity:0,y:900},{opacity:1,y:0}), 0);
                stage3.append(TweenMax.from(ST+"q", .4,      {opacity:0,x:900}), 0);
                stage3.append(TweenMax.from(ST+"Group3", .4,          {opacity:0,x:900}), 0);
                stage3.append(TweenMax.from(ST+"Recurso_38", .4,      {opacity:0,y:900}), -1);
                stage3.append(TweenMax.from(ST+"btn_instruccion", .4, {opacity:0,y:900}), -1);
                stage3.append(TweenMax.from(ST+"btn_creditos2", .8,   {y:300,opacity:0}), 0);
                stage3.stop();

                stage4 = new TimelineMax({onStart:function(){
                    ivo.play("a3");
                    Scorm_mx = new MX_SCORM(false);
                    console.log("Nombre: " + Scorm_mx.info_user().name + " Id: " + Scorm_mx.info_user().id+ " Nota: "+50);
                    Scorm_mx.set_score(50);
                }});
                stage4.append(TweenMax.fromTo(ST+"stage4", .4,        {opacity:0,y:900},{opacity:1,y:0}), 0);
                stage4.append(TweenMax.from(ST+"Recurso_41", .8,      {x:1300,opacity:0}), 0);
                stage4.append(TweenMax.from(ST+"Group", .8,           {x:1300,opacity:0}), 0);
                stage4.stop();

                creditos = new TimelineMax();
                creditos.append(TweenMax.from(ST+"creditos", .8,      {x:1300,opacity:0}), 0);
                creditos.append(TweenMax.from(ST+"btn_cerrar2", .8,   {scale:0,opacity:0,rotation:900}), 0);
                creditos.stop();

                pop = new TimelineMax();
                pop.append(TweenMax.from(ST+"msg", .8,                {x:1300,opacity:0}), 0);
                pop.append(TweenMax.from(ST+"Group5", .8,             {x:1300,opacity:0}), 0);
                pop.append(TweenMax.from(ST+"btn_cerrar", .8,         {scale:0,opacity:0,rotation:900}), 0);
                pop.stop();

            }
        }
 });
}
