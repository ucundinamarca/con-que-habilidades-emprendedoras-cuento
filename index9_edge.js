/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='images/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "both",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'stage1',
                            type: 'group',
                            rect: ['67', '44', '894', '602', 'auto', 'auto'],
                            c: [
                            {
                                id: 'Recurso_8',
                                type: 'image',
                                rect: ['267px', '124px', '396px', '396px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_8.png",'0px','0px']
                            },
                            {
                                id: 'Recurso_9',
                                type: 'image',
                                rect: ['380px', '88px', '187px', '514px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_9.png",'0px','0px']
                            },
                            {
                                id: 'Recurso_7',
                                type: 'image',
                                rect: ['0px', '510px', '45px', '39px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_7.png",'0px','0px'],
                                userClass: "destelloz"
                            },
                            {
                                id: 'Recurso_6',
                                type: 'image',
                                rect: ['835px', '482px', '32px', '28px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_6.png",'0px','0px'],
                                userClass: "destelloz"
                            },
                            {
                                id: 'Recurso_5',
                                type: 'image',
                                rect: ['223px', '415px', '38px', '45px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_5.png",'0px','0px'],
                                userClass: "destelloz"
                            },
                            {
                                id: 'Recurso_4',
                                type: 'image',
                                rect: ['719px', '257px', '32px', '28px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_4.png",'0px','0px'],
                                userClass: "destelloz"
                            },
                            {
                                id: 'Recurso_3',
                                type: 'image',
                                rect: ['851px', '152px', '43px', '38px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_3.png",'0px','0px'],
                                userClass: "destelloz"
                            },
                            {
                                id: 'Recurso_2',
                                type: 'image',
                                rect: ['51px', '152px', '38px', '45px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_2.png",'0px','0px'],
                                userClass: "destelloz"
                            },
                            {
                                id: 'Recurso_1',
                                type: 'image',
                                rect: ['35px', '0px', '856px', '52px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_1.png",'0px','0px']
                            },
                            {
                                id: 'btn_sig',
                                type: 'image',
                                rect: ['648px', '458px', '206px', '48px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"Recurso_10.png",'0px','0px']
                            }]
                        },
                        {
                            id: 'stage2',
                            type: 'group',
                            rect: ['26', '33', '982', '608', 'auto', 'auto'],
                            c: [
                            {
                                id: 'btn_creditos',
                                type: 'image',
                                rect: ['298px', '557px', '137px', '50px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"Recurso_13.png",'0px','0px']
                            },
                            {
                                id: 'btn_sig2',
                                type: 'image',
                                rect: ['744px', '560px', '206px', '48px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"Recurso_14.png",'0px','0px']
                            },
                            {
                                id: 'Recurso_12',
                                type: 'image',
                                rect: ['26px', '75px', '208px', '519px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_12.png",'0px','0px']
                            },
                            {
                                id: 'Recurso_11',
                                type: 'image',
                                rect: ['0px', '0px', '982px', '47px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_11.png",'0px','0px']
                            },
                            {
                                id: 'box',
                                type: 'group',
                                rect: ['296px', '114px', '659', '426', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'btn_ins',
                                    type: 'image',
                                    rect: ['0px', '184px', '312px', '72px', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_17.png",'0px','0px'],
                                    userClass: "btn"
                                },
                                {
                                    id: 'btn_con',
                                    type: 'image',
                                    rect: ['0px', '103px', '312px', '72px', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_16.png",'0px','0px'],
                                    userClass: "btn"
                                },
                                {
                                    id: 'btn_obj',
                                    type: 'image',
                                    rect: ['0px', '21px', '312px', '72px', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_15.png",'0px','0px'],
                                    userClass: "btn"
                                },
                                {
                                    id: 'Recurso_18',
                                    type: 'image',
                                    rect: ['238px', '0px', '421px', '426px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_18.png",'0px','0px']
                                },
                                {
                                    id: 'info',
                                    type: 'text',
                                    rect: ['260px', '19px', '379px', '390px', 'auto', 'auto'],
                                    text: "<p style=\"margin: 0px;\">​</p>",
                                    align: "left",
                                    userClass: "regular",
                                    font: ['Arial, Helvetica, sans-serif', [12, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "normal"],
                                    textStyle: ["", "", "", "", "none"]
                                }]
                            }]
                        },
                        {
                            id: 'stage3',
                            type: 'group',
                            rect: ['26', '33', '982', '590', 'auto', 'auto'],
                            c: [
                            {
                                id: 'Recurso_11Copy',
                                type: 'image',
                                rect: ['0px', '0px', '982px', '47px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_11.png",'0px','0px']
                            },
                            {
                                id: 'q',
                                type: 'group',
                                rect: ['335', '106', '636', '152', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'Recurso_21',
                                    type: 'image',
                                    rect: ['0px', '0px', '636px', '152px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_21.png",'0px','0px']
                                },
                                {
                                    id: '_q',
                                    type: 'text',
                                    rect: ['92px', '41px', '523px', '97px', 'auto', 'auto'],
                                    text: "<p style=\"margin: 0px;\">​</p>",
                                    userClass: "regular",
                                    font: ['Arial, Helvetica, sans-serif', [14, "px"], "rgba(0,0,0,1)", "normal", "none", "", "break-word", "normal"]
                                },
                                {
                                    id: 's',
                                    type: 'text',
                                    rect: ['91px', '4px', '419px', '27px', 'auto', 'auto'],
                                    text: "<p style=\"margin: 0px;\">​</p>",
                                    align: "left",
                                    userClass: "titulo",
                                    font: ['Arial, Helvetica, sans-serif', [18, "px"], "rgba(1,35,76,1.00)", "900", "none", "normal", "break-word", "normal"],
                                    textStyle: ["", "", "", "", "none"]
                                },
                                {
                                    id: 'n',
                                    type: 'text',
                                    rect: ['5px', '54px', '61px', '66px', 'auto', 'auto'],
                                    text: "<p style=\"margin: 0px; text-align: center;\">​<span style=\"font-size: 40px; color: rgb(255, 255, 255);\">1</span></p>",
                                    align: "left",
                                    userClass: "n",
                                    font: ['Arial, Helvetica, sans-serif', [51, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "normal"],
                                    textStyle: ["", "", "", "", "none"]
                                }]
                            },
                            {
                                id: 'Recurso_38',
                                type: 'image',
                                rect: ['13px', '55px', '234px', '543px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_38.png",'0px','0px']
                            },
                            {
                                id: 'Group3',
                                type: 'group',
                                rect: ['221', '273', '746', '264', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'f1',
                                    type: 'image',
                                    rect: ['22px', '0px', '445px', '76px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_27.png",'0px','0px']
                                },
                                {
                                    id: 'f2',
                                    type: 'image',
                                    rect: ['22px', '93px', '445px', '76px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_27.png",'0px','0px']
                                },
                                {
                                    id: 'f3',
                                    type: 'image',
                                    rect: ['22px', '188px', '445px', '76px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_27.png",'0px','0px']
                                },
                                {
                                    id: 'Recurso_24',
                                    type: 'image',
                                    rect: ['0px', '15px', '48px', '50px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_24.png",'0px','0px']
                                },
                                {
                                    id: 'Recurso_23',
                                    type: 'image',
                                    rect: ['0px', '106px', '48px', '51px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_23.png",'0px','0px']
                                },
                                {
                                    id: 'Recurso_22',
                                    type: 'image',
                                    rect: ['0px', '202px', '48px', '50px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_22.png",'0px','0px']
                                },
                                {
                                    id: 'fila1',
                                    type: 'group',
                                    rect: ['496px', '8px', '250', '55', 'auto', 'auto'],
                                    c: [
                                    {
                                        id: 'btn_1_',
                                        type: 'image',
                                        rect: ['0px', '1px', '50px', '54px', 'auto', 'auto'],
                                        cursor: 'pointer',
                                        fill: ["rgba(0,0,0,0)",im+"Recurso_36.png",'0px','0px']
                                    },
                                    {
                                        id: 'btn-1_',
                                        type: 'image',
                                        rect: ['0px', '1px', '50px', '54px', 'auto', 'auto'],
                                        cursor: 'pointer',
                                        fill: ["rgba(0,0,0,0)",im+"Recurso_32.png",'0px','0px'],
                                        userClass: "botones1"
                                    },
                                    {
                                        id: 'btn_2_',
                                        type: 'image',
                                        rect: ['65px', '0px', '51px', '54px', 'auto', 'auto'],
                                        cursor: 'pointer',
                                        fill: ["rgba(0,0,0,0)",im+"Recurso_35.png",'0px','0px']
                                    },
                                    {
                                        id: 'btn-2_',
                                        type: 'image',
                                        rect: ['65px', '0px', '51px', '54px', 'auto', 'auto'],
                                        cursor: 'pointer',
                                        fill: ["rgba(0,0,0,0)",im+"Recurso_31.png",'0px','0px'],
                                        userClass: "botones1"
                                    },
                                    {
                                        id: 'btn_3_',
                                        type: 'image',
                                        rect: ['130px', '0px', '51px', '54px', 'auto', 'auto'],
                                        cursor: 'pointer',
                                        fill: ["rgba(0,0,0,0)",im+"Recurso_34.png",'0px','0px']
                                    },
                                    {
                                        id: 'btn-3_',
                                        type: 'image',
                                        rect: ['130px', '0px', '51px', '54px', 'auto', 'auto'],
                                        cursor: 'pointer',
                                        fill: ["rgba(0,0,0,0)",im+"Recurso_30.png",'0px','0px'],
                                        userClass: "botones1"
                                    },
                                    {
                                        id: 'btn_4_',
                                        type: 'image',
                                        rect: ['195px', '0px', '51px', '54px', 'auto', 'auto'],
                                        cursor: 'pointer',
                                        fill: ["rgba(0,0,0,0)",im+"Recurso_33.png",'0px','0px']
                                    },
                                    {
                                        id: 'btn-4_',
                                        type: 'image',
                                        rect: ['195px', '0px', '51px', '54px', 'auto', 'auto'],
                                        cursor: 'pointer',
                                        fill: ["rgba(0,0,0,0)",im+"Recurso_29.png",'0px','0px'],
                                        userClass: "botones1"
                                    }]
                                },
                                {
                                    id: 'fila2',
                                    type: 'group',
                                    rect: ['496px', '103px', '250', '55', 'auto', 'auto'],
                                    c: [
                                    {
                                        id: 'btn_1__',
                                        type: 'image',
                                        rect: ['0px', '1px', '50px', '54px', 'auto', 'auto'],
                                        cursor: 'pointer',
                                        fill: ["rgba(0,0,0,0)",im+"Recurso_36.png",'0px','0px']
                                    },
                                    {
                                        id: 'btn-1__',
                                        type: 'image',
                                        rect: ['0px', '1px', '50px', '54px', 'auto', 'auto'],
                                        cursor: 'pointer',
                                        fill: ["rgba(0,0,0,0)",im+"Recurso_32.png",'0px','0px'],
                                        userClass: "botones2"
                                    },
                                    {
                                        id: 'btn_2__',
                                        type: 'image',
                                        rect: ['65px', '0px', '51px', '54px', 'auto', 'auto'],
                                        cursor: 'pointer',
                                        fill: ["rgba(0,0,0,0)",im+"Recurso_35.png",'0px','0px']
                                    },
                                    {
                                        id: 'btn-2__',
                                        type: 'image',
                                        rect: ['65px', '0px', '51px', '54px', 'auto', 'auto'],
                                        cursor: 'pointer',
                                        fill: ["rgba(0,0,0,0)",im+"Recurso_31.png",'0px','0px'],
                                        userClass: "botones2"
                                    },
                                    {
                                        id: 'btn_3__',
                                        type: 'image',
                                        rect: ['130px', '0px', '51px', '54px', 'auto', 'auto'],
                                        cursor: 'pointer',
                                        fill: ["rgba(0,0,0,0)",im+"Recurso_34.png",'0px','0px']
                                    },
                                    {
                                        id: 'btn-3__',
                                        type: 'image',
                                        rect: ['130px', '0px', '51px', '54px', 'auto', 'auto'],
                                        cursor: 'pointer',
                                        fill: ["rgba(0,0,0,0)",im+"Recurso_30.png",'0px','0px'],
                                        userClass: "botones2"
                                    },
                                    {
                                        id: 'btn_4__',
                                        type: 'image',
                                        rect: ['195px', '0px', '51px', '54px', 'auto', 'auto'],
                                        cursor: 'pointer',
                                        fill: ["rgba(0,0,0,0)",im+"Recurso_33.png",'0px','0px']
                                    },
                                    {
                                        id: 'btn-4__',
                                        type: 'image',
                                        rect: ['195px', '0px', '51px', '54px', 'auto', 'auto'],
                                        cursor: 'pointer',
                                        fill: ["rgba(0,0,0,0)",im+"Recurso_29.png",'0px','0px'],
                                        userClass: "botones2"
                                    }]
                                },
                                {
                                    id: 'fila3',
                                    type: 'group',
                                    rect: ['496px', '196px', '250', '55', 'auto', 'auto'],
                                    c: [
                                    {
                                        id: 'btn_1___',
                                        type: 'image',
                                        rect: ['0px', '1px', '50px', '54px', 'auto', 'auto'],
                                        cursor: 'pointer',
                                        fill: ["rgba(0,0,0,0)",im+"Recurso_36.png",'0px','0px']
                                    },
                                    {
                                        id: 'btn-1___',
                                        type: 'image',
                                        rect: ['0px', '1px', '50px', '54px', 'auto', 'auto'],
                                        cursor: 'pointer',
                                        fill: ["rgba(0,0,0,0)",im+"Recurso_32.png",'0px','0px'],
                                        userClass: "botones3"
                                    },
                                    {
                                        id: 'btn_2___',
                                        type: 'image',
                                        rect: ['65px', '0px', '51px', '54px', 'auto', 'auto'],
                                        cursor: 'pointer',
                                        fill: ["rgba(0,0,0,0)",im+"Recurso_35.png",'0px','0px']
                                    },
                                    {
                                        id: 'btn-2___',
                                        type: 'image',
                                        rect: ['65px', '0px', '51px', '54px', 'auto', 'auto'],
                                        cursor: 'pointer',
                                        fill: ["rgba(0,0,0,0)",im+"Recurso_31.png",'0px','0px'],
                                        userClass: "botones3"
                                    },
                                    {
                                        id: 'btn_3___',
                                        type: 'image',
                                        rect: ['130px', '0px', '51px', '54px', 'auto', 'auto'],
                                        cursor: 'pointer',
                                        fill: ["rgba(0,0,0,0)",im+"Recurso_34.png",'0px','0px']
                                    },
                                    {
                                        id: 'btn-3___',
                                        type: 'image',
                                        rect: ['130px', '0px', '51px', '54px', 'auto', 'auto'],
                                        cursor: 'pointer',
                                        fill: ["rgba(0,0,0,0)",im+"Recurso_30.png",'0px','0px'],
                                        userClass: "botones3"
                                    },
                                    {
                                        id: 'btn_4___',
                                        type: 'image',
                                        rect: ['195px', '0px', '51px', '54px', 'auto', 'auto'],
                                        cursor: 'pointer',
                                        fill: ["rgba(0,0,0,0)",im+"Recurso_33.png",'0px','0px']
                                    },
                                    {
                                        id: 'btn-4___',
                                        type: 'image',
                                        rect: ['195px', '0px', '51px', '54px', 'auto', 'auto'],
                                        cursor: 'pointer',
                                        fill: ["rgba(0,0,0,0)",im+"Recurso_29.png",'0px','0px'],
                                        userClass: "botones3"
                                    }]
                                },
                                {
                                    id: 'btn_evaluar',
                                    type: 'image',
                                    rect: ['539px', '269px', '206px', '48px', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_37.png",'0px','0px']
                                },
                                {
                                    id: 't1',
                                    type: 'text',
                                    rect: ['53px', '11px', '397px', '58px', 'auto', 'auto'],
                                    text: "<p style=\"margin: 0px;\">​</p>",
                                    align: "left",
                                    userClass: "regular",
                                    font: ['Arial, Helvetica, sans-serif', [12, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "normal"],
                                    textStyle: ["", "", "", "", "none"]
                                },
                                {
                                    id: 't2',
                                    type: 'text',
                                    rect: ['53px', '101px', '397px', '58px', 'auto', 'auto'],
                                    text: "<p style=\"margin: 0px;\">​</p>",
                                    align: "left",
                                    userClass: "regular",
                                    font: ['Arial, Helvetica, sans-serif', [12, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "normal"],
                                    textStyle: ["", "", "", "", "none"]
                                },
                                {
                                    id: 't3',
                                    type: 'text',
                                    rect: ['53px', '196px', '397px', '58px', 'auto', 'auto'],
                                    text: "<p style=\"margin: 0px;\">​</p>",
                                    align: "left",
                                    userClass: "regular",
                                    font: ['Arial, Helvetica, sans-serif', [12, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "normal"],
                                    textStyle: ["", "", "", "", "none"]
                                }]
                            },
                            {
                                id: 'btn_play',
                                type: 'image',
                                rect: ['148px', '535px', '48px', '50px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"Recurso_28.png",'0px','0px']
                            },
                            {
                                id: 'btn_instruccion',
                                type: 'image',
                                rect: ['786px', '54px', '195px', '45px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"Recurso_20.png",'0px','0px']
                            },
                            {
                                id: 'Group4',
                                type: 'group',
                                rect: ['366', '281', '602', '317', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'Recurso_393',
                                    type: 'image',
                                    rect: ['217px', '0px', '255px', '75px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_393.png",'0px','0px']
                                },
                                {
                                    id: 'Recurso_40',
                                    type: 'image',
                                    rect: ['0px', '86px', '602px', '174px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_40.png",'0px','0px']
                                },
                                {
                                    id: 'btn_sig3',
                                    type: 'image',
                                    rect: ['393px', '272px', '206px', '48px', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_14.png",'0px','0px']
                                },
                                {
                                    id: 'nd',
                                    type: 'text',
                                    rect: ['4px', '21px', 'auto', 'auto', 'auto', 'auto'],
                                    text: "<p style=\"margin: 0px;\">​7</p>",
                                    align: "left",
                                    userClass: "titulo",
                                    font: ['Arial, Helvetica, sans-serif', [70, "px"], "rgba(1,35,76,1.00)", "400", "none", "normal", "break-word", "nowrap"],
                                    textStyle: ["", "", "", "", "none"]
                                },
                                {
                                    id: 'Text2',
                                    type: 'text',
                                    rect: ['72px', '8px', '137px', '64px', 'auto', 'auto'],
                                    text: "<p style=\"margin: 0px;\">​Puntos</p><p style=\"margin: 0px;\">obtenidos<span style=\"font-size: 12px;\">​</span></p>",
                                    align: "left",
                                    userClass: "titulo",
                                    font: ['Arial, Helvetica, sans-serif', [22, "px"], "rgba(1,35,76,1.00)", "400", "none", "normal", "break-word", "normal"],
                                    textStyle: ["", "", "", "", "none"]
                                },
                                {
                                    id: 'Text3',
                                    type: 'text',
                                    rect: ['19px', '134px', '562px', '108px', 'auto', 'auto'],
                                    text: "<p style=\"margin: 0px;\">​</p>",
                                    align: "left",
                                    userClass: "regular",
                                    font: ['Arial, Helvetica, sans-serif', [22, "px"], "rgba(1,35,76,1)", "400", "none", "normal", "break-word", "normal"],
                                    textStyle: ["", "", "", "", "none"]
                                },
                                {
                                    id: 'Text4',
                                    type: 'text',
                                    rect: ['228px', '21px', '234px', '36px', 'auto', 'auto'],
                                    text: "<p style=\"margin: 0px; text-align: center;\">​<span style=\"font-size: 30px;\">Nivel Bajo</span></p>",
                                    align: "left",
                                    userClass: "regular",
                                    font: ['Arial, Helvetica, sans-serif', [22, "px"], "rgba(1,35,76,1)", "400", "none", "normal", "break-word", "normal"],
                                    textStyle: ["", "", "", "", "none"]
                                },
                                {
                                    id: 'retro',
                                    type: 'text',
                                    rect: ['15px', '136px', '574px', '116px', 'auto', 'auto'],
                                    text: "<p style=\"margin: 0px;\">​</p>",
                                    align: "justify",
                                    userClass: "regular",
                                    font: ['Arial, Helvetica, sans-serif', [13, "px"], "rgba(1,35,76,1)", "400", "none", "normal", "break-word", "normal"],
                                    textStyle: ["", "", "14px", "", "none"]
                                }]
                            }]
                        },
                        {
                            id: 'stage4',
                            type: 'group',
                            rect: ['18px', '30px', '982', '598', 'auto', 'auto'],
                            c: [
                            {
                                id: 'Rectangle',
                                type: 'rect',
                                rect: ['-18px', '-30px', '1024px', '640px', 'auto', 'auto'],
                                fill: ["rgba(255,255,255,1.00)"],
                                stroke: [0,"rgba(0,0,0,1)","none"]
                            },
                            {
                                id: 'Recurso_44',
                                type: 'image',
                                rect: ['93px', '69px', '353px', '529px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_44.png",'0px','0px']
                            },
                            {
                                id: 'Recurso_41',
                                type: 'image',
                                rect: ['415px', '186px', '505px', '55px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_41.png",'0px','0px']
                            },
                            {
                                id: 'Recurso_11Copy2',
                                type: 'image',
                                rect: ['0px', '0px', '982px', '47px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_11.png",'0px','0px']
                            },
                            {
                                id: 'Group',
                                type: 'group',
                                rect: ['366', '274', '603', '130', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'Recurso_42',
                                    type: 'image',
                                    rect: ['0px', '0px', '603px', '130px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_42.png",'0px','0px']
                                },
                                {
                                    id: 'Text6',
                                    type: 'text',
                                    rect: ['18px', '18px', '562px', '97px', 'auto', 'auto'],
                                    text: "<p style=\"margin: 0px;\">​<span style=\"font-size: 18px;\">¡Ha finalizado su autodiagnóstico! Ahora tiene un poco más claro cuáles son esas habilidades blandas que puede potenciar en su camino de emprendimiento y cuáles puede desarrollar aún. Ha dado un gran paso.</span><span style=\"font-size: 18px; font-weight: 800;\"> ¡Adelante!</span></p>",
                                    align: "left",
                                    userClass: "regular",
                                    font: ['Arial, Helvetica, sans-serif', [14, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "normal"],
                                    textStyle: ["", "", "", "", "none"]
                                }]
                            }]
                        },
                        {
                            id: 'creditos',
                            type: 'group',
                            rect: ['0', '2', '1024', '640', 'auto', 'auto'],
                            c: [
                            {
                                id: 'Recurso_45',
                                type: 'image',
                                rect: ['0px', '0px', '1024px', '640px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_45.png",'0px','0px']
                            },
                            {
                                id: 'btn_cerrar2',
                                type: 'image',
                                rect: ['934px', '13px', '76px', '76px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"btn_cerrar.png",'0px','0px']
                            }]
                        },
                        {
                            id: 'msg',
                            type: 'group',
                            rect: ['267', '211', '483', '212', 'auto', 'auto'],
                            c: [
                            {
                                id: 'Rectangle2',
                                type: 'rect',
                                rect: ['-267px', '-211px', '1024px', '640px', 'auto', 'auto'],
                                fill: ["rgba(33,33,33,0.53)"],
                                stroke: [0,"rgba(0,0,0,1)","none"]
                            },
                            {
                                id: 'Group5',
                                type: 'group',
                                rect: ['0', '0', '483', '212', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'cuadro-instru',
                                    type: 'image',
                                    rect: ['0px', '0px', '483px', '212px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"cuadro-instru.png",'0px','0px']
                                },
                                {
                                    id: 'Text',
                                    type: 'text',
                                    rect: ['33px', '44px', '419px', '152px', 'auto', 'auto'],
                                    text: "<p style=\"margin: 0px;\"><span style=\"font-weight: 900;\">Instrucción</span></p><p style=\"margin: 0px;\"><span style=\"font-size: 16px;\">​</span></p><p style=\"margin: 0px;\"><span style=\"font-size: 16px;\">Lea con atención los distintos aspectos asociados a las habilidades blandas que se le presentan a continuación e indique con honestidad qué tanto se identifica usted con cada uno de éstos. En cada caso marque 1 si no se identifica en absoluto y 4 si lo define completamente.&nbsp;</span></p><p style=\"margin: 0px;\">​</p>",
                                    align: "justify",
                                    font: ['Arial, Helvetica, sans-serif', [24, ""], "rgba(0,0,0,1)", "normal", "none", "", "break-word", "normal"],
                                    textStyle: ["", "", "13px", "", ""]
                                }]
                            },
                            {
                                id: 'btn_cerrar',
                                type: 'image',
                                rect: ['425px', '22px', '38px', '38px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"btn_cerrar.png",'0px','0px']
                            }]
                        },
                        {
                            id: 'preload',
                            type: 'group',
                            rect: ['0', '0', '1024', '640', 'auto', 'auto'],
                            c: [
                            {
                                id: 'Rectangle3',
                                type: 'rect',
                                rect: ['0px', '0px', '1024px', '640px', 'auto', 'auto'],
                                fill: ["rgba(255,255,255,1)"],
                                stroke: [0,"rgb(0, 0, 0)","none"]
                            },
                            {
                                id: 'logo',
                                type: 'image',
                                rect: ['462px', '272px', '100px', '100px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"logo.svg",'0px','0px']
                            }]
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '1024px', '640px', 'auto', 'auto'],
                            overflow: 'hidden',
                            fill: ["rgba(255,255,255,1)"]
                        }
                    }
                },
                timeline: {
                    duration: 0,
                    autoPlay: true,
                    data: [

                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

    if (!window.edge_authoring_mode) AdobeEdge.getComposition(compId).load("index9_edgeActions.js");
})("EDGE-263128572");
